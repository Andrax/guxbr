$(document).ready(function(){
	
	/*$("div.forum_post_item").each(function(i){
		id = $(this).attr("id");
		height = $("#"+id+" div.post_data").height();
		$("#"+id+" div.post_text").css("min-height",height+"px");
	});*/
	
	$("a.show_more").click(function(){
		id = $(this).attr("rel");
		data = id.replace("other-data-","");
		$("#post-text-"+data).css('width','430px');
		$("#post-data-"+data).css("width",'150px');
		$("#"+id).slideDown('slow', function(){
			height = $("#post-data-"+data).height();
			$("#post-text-"+data).css("min-height",height+"px");
		});
		$(this).hide();
		$("a.show_less[rel='"+$(this).attr('rel')+"']").show();
	});
	
	$("a.show_less").click(function(){
		id = $(this).attr("rel");
		$("#"+id).slideUp('slow', function(){
			data = id.replace("other-data-","");
			$("#post-text-"+data).css('width','500px');
			$("#post-data-"+data).css("width",'80px');
			height = $("#post-data-"+data).height();
			$("#post-text-"+data).css("min-height",height+"px");
		});
		$(this).hide();
		$("a.show_more[rel='"+$(this).attr('rel')+"']").show();
	});
	
});